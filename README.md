# Wi-Fi connection monitor for OS X
Apple MacBook pro wifi drivers/hardware has a lot compatibility issues with a
number of wifi routers.   
OS X doesn't automatically reconnect to the same erroneous wifi accesspoint after a disconnect.   
This utility monitors the wifi connection by pinging a target host (default=google.com) and if the connection is down the utility tries to automatically restore the connection. 

> This utility (=net-ping dependency) needs administrator privileges 

Usage
--------------
1. edit monitor.js and set the SSID and wifiPassword parameters
2. ```./monitor```

License
----

MIT