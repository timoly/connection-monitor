var monitor = function(){
    var ping = require ("net-ping"),
        exec = require('child_process').exec;

    var session = ping.createSession(),
        command = "networksetup -setairportnetwork ",
        check,
        connectionErrorCount = 0,
        pingTimeoutCount = 0;

    const maxPingTimeOutCount = 1;
        
    const options = {
        maxConnectionErrorCount: 50,
        targetUrl: "173.194.32.7", // google.com
        ssid: "wp8luumia",
        wifiPassword: "",
        wifiInterface: "en0"
    };
    console.log("options:", options);

    if(options.wifiPassword.length === 0 || options.ssid.length === 0){
        console.error("wifiPassword & ssid need to be set");
        process.exit(1);
    }
        
    command += options.wifiInterface + " " + options.ssid + " " + options.wifiPassword;
    console.log("connect command:", command);
    
    var connect = function myself(){
        console.log(new Date(), "connecting...");

        exec(command, function (error, stdout, stderr) {
            if(stdout){
                ++connectionErrorCount;
                console.log(new Date(), "Error reconnecting", stdout, "errorCount:", connectionErrorCount);
                if(connectionErrorCount < options.maxConnectionErrorCount){
                    myself();
                }
                else{
                    console.error("connectionErrorCount reached, exiting");
                    process.exit(1);
                }
            }
            else{
                console.log(new Date(), "successfully reconnected");
                check();
            }
        });
    };

    check = function myself(){
        session.pingHost(options.targetUrl, function (error, target) {
            if (error && pingTimeoutCount < maxPingTimeOutCount){
                console.error(new Date(), error, "first ping timeout");
                ++pingTimeoutCount;
                setTimeout(myself, 1000);
            }
            else if(pingTimeoutCount >= maxPingTimeOutCount){
                console.log(new Date(), "second ping timeout, reconnecting...");
                pingTimeoutCount = 0;
                connect();
            }
            else{
                pingTimeoutCount = 0;
                connectionErrorCount = 0;
                setTimeout(myself, 1000);
            }
        });
    };
    check();
};

try{
    console.log(new Date(), "starting to monitor wifi connection...");
    monitor();
}
catch(err){
    console.error("err:", err);
    console.log("Check that you have admin privileges")
}